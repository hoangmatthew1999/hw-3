package P6_5.java;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.Scanner;

public class PrimeGenerator {
    public  ArrayList<Integer> nextPrime(int primeArgument){
        PrimeGenerator newPrimeInstance = new PrimeGenerator();
        ArrayList <Integer> returnArray = new ArrayList<Integer>();
        for(int i = 2; i <= primeArgument; i++){
            if(newPrimeInstance.isPrime(i) == true){
                System.out.println("The number is prime " + i);
                returnArray.add(i);

            }
        }
        return returnArray;
    }
    public boolean isPrime(int primeCandidateArgument){
        for(int i = 2; i < primeCandidateArgument;i++){
            if(primeCandidateArgument % i == 0){//number is not a prime number
                return false;
            }
        }
        return true;
    }
    public static void main(String[] args) {
        String choice = null;
        Scanner scan = new Scanner(System.in);
        do {
            System.out.println("Please enter a number to test if prime");
            choice = scan.nextLine();//need this to be able to have the exit case; if i did just created an int input
            if(choice.equals("q")) {
                break;
            }
            else{
                try{
                    int choiceInt = Integer.parseInt(choice);// how would i add 0 aka add nothing but subtract something
                    PrimeGenerator instance = new PrimeGenerator();
                    instance.isPrime(6);
                    instance.nextPrime(choiceInt);
                }
                catch(Exception e){System.out.println("not a valid number");}

            }

        }while(!choice.equals("q"));
    }
}
