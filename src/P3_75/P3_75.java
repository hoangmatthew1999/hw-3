package P3_75;
import java.util.Scanner;

public class P3_75 {
    public static int alterSum(){
        String choice = null;
        Scanner scan = new Scanner(System.in);
        int total = 0;
        String operation = "add";
        do {
            System.out.println("Enter a number or press lowercase q to quit");
            choice = scan.nextLine();//need this to be able to have the exit case; if i did just created an int input
            if(choice.equals("q")) {
                break;
            }
            else{
                try {
                    int choiceInt = Integer.parseInt(choice);// how would i add 0 aka add nothing but subtract something
                    if(operation == "add"){total = total + choiceInt;operation = "subtract";}
                    else{total = total - choiceInt;operation = "add";}
                    System.out.println(total);
                }
                catch(Exception e){System.out.println("not a valid number");}
            }
        } while (!choice.equals("q")); // end of loop

        return total;
    }
    public static void main(String[] args) {
        alterSum();
    }
}
